set terminal png
set datafile separator ","
set output "diagonal.png"
plot 'diagonal.csv' using 2:3 with lines lt rgb "blue"
