extern crate euclid;
extern crate fixed;
extern crate scaled;

use scaled::FixedSqrt;
use scaled::wrapped::{Point2d, Space2d, Vector2d, Wrapping};

macro_rules! show {
  ($e:expr) => { println!("{:<24}{:?}", format!("{}: ", stringify!($e)), $e); }
}

struct World;
impl Space2d for World {
  type Scalar = fixed::types::I16F16;
  type Point  = euclid::Point2D  <Wrapping <Self::Scalar>, Self>;
  type Vector = euclid::Vector2D <Wrapping <Self::Scalar>, Self>;
}
type Scalar = <World as Space2d>::Scalar;
type Point  = <World as Space2d>::Point;
type Vector = <World as Space2d>::Vector;

fn main() {
  use std::mem::size_of;
  println!("example main...");

  show!(size_of::<Scalar>());
  show!(size_of::<Point>());
  show!(size_of::<Vector>());

  show!(Scalar::MIN);
  show!(Scalar::MAX);
  show!(Scalar::MAX.sqrt());

  let x = Wrapping::from_num (1);
  let y = Wrapping::from_num (-1);
  show!(x);
  show!(x.to_bits());
  show!(y);
  show!(y.to_bits());
  show!(y + x);
  show!(y - x);
  let e = euclid::Point2D::new (1.0, 2.0);
  show!(e);
  show!(Point::from_num (e));
  let p = Point::new (x, y);
  show!(p);
  show!(p.to_bits());
  show!(p.to_num::<f64>());
  let v = Vector::new (x, y);
  show!(v);
  show!(v.to_bits());
  show!(v.to_num::<f64>());
  show!(v.square_length());
  show!(v.magnitude2());
  show!(v.magnitude());
  show!(v.normalized());
  let two = Wrapping::from_num (2);
  show!(v * two);
  show!(p + v);
  // euclid::Point2D::origin can't infer that Scalar : num_traits::Zero so we
  // have to use Point2d::origin instead
  let o : Point = Point2d::origin();
  show!(o);
  show!(o - p);
  let w = Vector::from_num ([127.0, 127.0].into());
  show!(w);
  show!(w.magnitude());
  show!(w.magnitude2());
  show!(w.normalized());
  let w = Vector::from_num ([256.0, 256.0].into());
  show!(w);
  show!(w.magnitude());
  show!(w.magnitude2());
  show!(w.normalized());

  let pmax = Point::new (Wrapping::MAX, Wrapping::MAX);
  let pmin = Point::new (Wrapping::MIN, Wrapping::MIN);
  show!(pmax);
  show!(pmin);
  show!(pmax - pmin);

  println!("...example main");
}
