extern crate euclid;
extern crate fixed;
extern crate scaled;

use scaled::*;

#[allow(unused_macros)]
macro_rules! show {
  ($e:expr) => { println!("{:<24}{:?}", format!("{}: ", stringify!($e)), $e); }
}

struct World;
impl Space2d for World {
  type Scalar = fixed::types::I4F4;
  //type Scalar = fixed::types::I8F56;
  type Point  = euclid::Point2D  <Self::Scalar, Self>;
  type Vector = euclid::Vector2D <Self::Scalar, Self>;
}
type Scalar = <World as Space2d>::Scalar;
//type Point  = <World as Space2d>::Point;
type Vector = <World as Space2d>::Vector;

fn main() {
  use std::io::Write;
  println!("norm example main...");

  let mut file = std::fs::File::create ("norm.csv").unwrap();
  let mut file_hist = std::fs::File::create ("hist.csv").unwrap();
  println!("writing norm.csv...");
  println!("writing hist.csv...");
  let mut x = Scalar::MIN;
  loop {
    let mut y = Scalar::MIN;
    loop {
      let (norm, color) = match Vector::new (x, y).norm() {
        Norm::Euclidean (n) => (n, 0),
        Norm::Taxicab   (n) => (n, 1),
        Norm::Maximum   (n) => (n, 2)
      };
      file.write (format!("{},{},{}\n", x, y, color).as_bytes()).unwrap();
      file_hist.write (format!("{}\n", norm).as_bytes()).unwrap();
      if y == Scalar::MAX {
        break
      }
      y += Scalar::from_bits (1);
      //y = y.saturating_add (Scalar::MAX / Scalar::from_num (120.0));
    }
    if x == Scalar::MAX {
      break
    }
    x += Scalar::from_bits (1);
    //x = x.saturating_add (Scalar::MAX / Scalar::from_num (120.0));
  }

  let mut file = std::fs::File::create ("diagonal.csv").unwrap();
  println!("writing diagonal.csv...");
  let mut x = Scalar::MIN;
  let mut y = Scalar::MIN;
  loop {
    let norm = match Vector::new (x, y).norm() {
      Norm::Euclidean (n) => n,
      Norm::Taxicab   (n) => n,
      Norm::Maximum   (n) => n
    };
    file.write (format!("{},{},{}\n", x, y, norm).as_bytes()).unwrap();
    if x == Scalar::MAX {
      debug_assert_eq!(y, Scalar::MAX);
      break
    }
    x += Scalar::from_bits (1);
    y += Scalar::from_bits (1);
  }

  println!("...norm example main");
}
