//! Euclid points and vectors with wrapped fixed-point scalars

use fixed::traits::{FixedSigned, FromFixed, ToFixed};
use fixed_sqrt::FixedSqrt;

pub use fixed::Wrapping;

#[allow(unused_macros)]
macro_rules! show {
  ($e:expr) => { println!("{}: {:?}", stringify!($e), $e); }
}

pub trait Space2d : Sized {
  type Scalar : FixedSqrt;
  type Point  : Point2d  <Self::Scalar, Self> =
    euclid::Point2D <Wrapping <Self::Scalar>, Self>;
  type Vector : Vector2d <Self::Scalar, Self> =
    euclid::Vector2D <Wrapping <Self::Scalar>, Self>;
}

pub trait Point2d <S : FixedSqrt, U> : Sized + Clone + Copy + Default {
  // required
  fn new (x : S, y : S) -> Self;
  fn map <F, T> (self, f : F) -> euclid::Point2D <T, U> where
    F : FnMut (Wrapping <S>) -> T;
  // provided
  fn origin() -> Self {
    Self::default()
  }
  fn from_num <N : ToFixed> (num : euclid::Point2D <N, U>) -> Self {
    Self::new (num.x.to_fixed(), num.y.to_fixed())
  }
  fn wrapping_from_num <N : ToFixed> (num : euclid::Point2D <N, U>) -> Self {
    Self::new (num.x.wrapping_to_fixed(), num.y.wrapping_to_fixed())
  }
  fn from_bits (bits : euclid::Point2D <S::Bits, U>) -> Self {
    Self::new (S::from_bits (bits.x), S::from_bits (bits.y))
  }
  fn to_num <N : FromFixed> (self) -> euclid::Point2D <N, U> {
    self.map (|s| S::to_num (s.0))
  }
  fn to_bits (self) -> euclid::Point2D <S::Bits, U> {
    self.map (|s| S::to_bits (s.0))
  }
}

pub trait Vector2d <S : FixedSqrt, U> : Sized + Clone + Copy + Default {
  // required
  fn new (x : S, y : S) -> Self;
  fn map <F, T> (self, f : F) -> euclid::Vector2D <T, U> where
    F : FnMut (Wrapping <S>) -> T;
  fn magnitude2 (self) -> Option <Wrapping <S>>;
  fn norm (self) -> Norm <Wrapping <S>> where S : FixedSigned;
  // provided
  fn zero() -> Self {
    Self::default()
  }
  fn from_num <N : ToFixed> (num : euclid::Vector2D <N, U>) -> Self {
    Self::new (num.x.to_fixed(), num.y.to_fixed())
  }
  fn wrapping_from_num <N : ToFixed> (num : euclid::Vector2D <N, U>) -> Self {
    Self::new (num.x.wrapping_to_fixed(), num.y.wrapping_to_fixed())
  }
  fn from_bits (bits : euclid::Vector2D <S::Bits, U>) -> Self {
    Self::new (S::from_bits (bits.x), S::from_bits (bits.y))
  }
  fn to_num <N : FromFixed> (self) -> euclid::Vector2D <N, U> {
    self.map (|s| N::from_fixed (s.0))
  }
  fn to_bits (self) -> euclid::Vector2D <S::Bits, U> {
    self.map (|s| S::to_bits (s.0))
  }
  fn scale (self, s : Wrapping <S>) -> Self {
    let new = self.map (|x| x * s);
    Self::new (new.x.0, new.y.0)
  }
  fn magnitude (self) -> Option <Wrapping <S>> {
    self.magnitude2().map (|s| FixedSqrt::sqrt (s.0)).map (Wrapping)
  }
  fn normalized (mut self) -> Self {
    let magnitude = loop {
      match self.magnitude() {
        Some (s) => break s,
        None => {
          let new = self.map (|s| s.0 >> 1);
          self = Self::new (new.x, new.y);
        }
      }
    };
    // TODO: make this static or const ?
    let one = Wrapping (S::from_num (1usize));
    self.scale (one / magnitude)
  }
}

impl <S, U> Point2d <S, U> for euclid::Point2D <Wrapping <S>, U> where
  S : FixedSqrt
{
  fn new (x : S, y : S) -> Self {
    euclid::Point2D::new (Wrapping (x), Wrapping (y))
  }
  fn map <F, T> (self, mut f : F) -> euclid::Point2D <T, U> where
    F : FnMut (Wrapping <S>) -> T
  {
    euclid::Point2D::new (f (self.x), f (self.y))
  }
}

impl <S, U> Vector2d <S, U> for euclid::Vector2D <Wrapping <S>, U> where
  S : FixedSqrt
{
  fn new (x : S, y : S) -> Self {
    euclid::Vector2D::new (Wrapping (x), Wrapping (y))
  }
  fn map <F, T> (self, mut f : F) -> euclid::Vector2D <T, U> where
    F : FnMut (Wrapping <S>) -> T
  {
    euclid::Vector2D::new (f (self.x), f (self.y))
  }
  fn magnitude2 (self) -> Option <Wrapping <S>> {
    let x2 = self.x.0.checked_mul (self.x.0)?;
    let y2 = self.y.0.checked_mul (self.y.0)?;
    x2.checked_add (y2).map (Wrapping)
  }
  fn norm (self) -> Norm <Wrapping <S>> where S : FixedSigned {
    if let Some (magnitude) = self.magnitude() {
      return Norm::Euclidean (magnitude)
    }
    if let Some (sum) = self.x.0.checked_abs()
      .and_then (|x| self.y.0.checked_abs().and_then (|y| x.checked_add (y)))
    {
      return Norm::Taxicab (Wrapping (sum))
    }
    Norm::Maximum (
      Wrapping (self.x.0.saturating_abs().max (self.y.0.saturating_abs())))
  }
}

pub enum Norm <S> {
  /// 2-norm
  Euclidean (S),
  /// 1-norm
  Taxicab   (S),
  /// Infinity-norm
  Maximum   (S)
}

#[cfg(test)]
mod tests {
  use fixed;
  use euclid;
  use super::*;

  #[test]
  fn test_overflow() {
    struct World;
    impl Space2d for World {
      type Scalar = fixed::types::I4F4;
      type Point  = euclid::Point2D  <Wrapping <Self::Scalar>, Self>;
      type Vector = euclid::Vector2D <Wrapping <Self::Scalar>, Self>;
    }
    type Scalar = <World as Space2d>::Scalar;
    type Point  = <World as Space2d>::Point;
    //type Vector = <World as Space2d>::Vector;

    let p = Point::new (Wrapping (Scalar::MIN), Wrapping (Scalar::MIN));
    let q = Point::new (Wrapping (Scalar::MAX), Wrapping (Scalar::MAX));
    show!(p);
    show!(q);
    show!(q - p);
    show!(p - q);
  }

  #[test]
  fn test_norm_positive() {
    struct World;
    impl Space2d for World {
      type Scalar = fixed::types::I4F4;
      type Point  = euclid::Point2D  <Wrapping <Self::Scalar>, Self>;
      type Vector = euclid::Vector2D <Wrapping <Self::Scalar>, Self>;
    }
    type Scalar = <World as Space2d>::Scalar;
    //type Point  = <World as Space2d>::Point;
    type Vector = <World as Space2d>::Vector;
    let mut x = Scalar::MIN;
    loop {
      let mut y = Scalar::MIN;
      loop {
        let norm = match Vector::new (Wrapping (x), Wrapping (y)).norm() {
          Norm::Euclidean (n) => n,
          Norm::Taxicab   (n) => n,
          Norm::Maximum   (n) => n
        };
        show!(norm);
        assert!(norm >= Wrapping (Scalar::from_bits (0)));
        if y == Scalar::MAX {
          break
        }
        y += Scalar::from_bits (1);
      }
      if x == Scalar::MAX {
        break
      }
      x += Scalar::from_bits (1);
    }
  }

}
