//! Euclid points and vectors with fixed-point scalars

#![feature(associated_type_defaults)]

use fixed::traits::{FixedSigned, FromFixed, ToFixed};
pub use fixed_sqrt::FixedSqrt;

#[allow(unused_macros)]
macro_rules! show {
  ($e:expr) => { println!("{}: {:?}", stringify!($e), $e); }
}

pub mod wrapped;

pub trait Space2d : Sized {
  type Scalar : FixedSqrt;
  type Point  : Point2d  <Self::Scalar, Self> =
    euclid::Point2D <Self::Scalar, Self>;
  type Vector : Vector2d <Self::Scalar, Self> =
    euclid::Vector2D <Self::Scalar, Self>;
}

pub trait Point2d <S : FixedSqrt, U> : Sized + Clone + Copy + Default {
  // required
  fn new (x : S, y : S) -> Self;
  fn map <F, T> (self, f : F) -> euclid::Point2D <T, U> where
    F : FnMut (S) -> T;
  // provided
  fn origin() -> Self {
    Self::default()
  }
  fn from_num <N : ToFixed> (num : euclid::Point2D <N, U>) -> Self {
    Self::new (num.x.to_fixed(), num.y.to_fixed())
  }
  fn wrapping_from_num <N : ToFixed> (num : euclid::Point2D <N, U>) -> Self {
    Self::new (num.x.wrapping_to_fixed(), num.y.wrapping_to_fixed())
  }
  fn from_bits (bits : euclid::Point2D <S::Bits, U>) -> Self {
    Self::new (S::from_bits (bits.x), S::from_bits (bits.y))
  }
  fn to_num <N : FromFixed> (self) -> euclid::Point2D <N, U> {
    self.map (S::to_num)
  }
  fn to_bits (self) -> euclid::Point2D <S::Bits, U> {
    self.map (S::to_bits)
  }
}

pub trait Vector2d <S : FixedSqrt, U> : Sized + Clone + Copy + Default {
  // required
  fn new (x : S, y : S) -> Self;
  fn map <F, T> (self, f : F) -> euclid::Vector2D <T, U> where
    F : FnMut (S) -> T;
  fn magnitude2 (self) -> Option <S>;
  fn norm (self) -> Norm <S> where S : FixedSigned;
  // provided
  fn zero() -> Self {
    Self::default()
  }
  fn from_num <N : ToFixed> (num : euclid::Vector2D <N, U>) -> Self {
    Self::new (num.x.to_fixed(), num.y.to_fixed())
  }
  fn wrapping_from_num <N : ToFixed> (num : euclid::Vector2D <N, U>) -> Self {
    Self::new (num.x.wrapping_to_fixed(), num.y.wrapping_to_fixed())
  }
  fn from_bits (bits : euclid::Vector2D <S::Bits, U>) -> Self {
    Self::new (S::from_bits (bits.x), S::from_bits (bits.y))
  }
  fn to_num <N : FromFixed> (self) -> euclid::Vector2D <N, U> {
    self.map (N::from_fixed)
  }
  fn to_bits (self) -> euclid::Vector2D <S::Bits, U> {
    self.map (S::to_bits)
  }
  fn scale (self, s : S) -> Self {
    let new = self.map (|x| x * s);
    Self::new (new.x, new.y)
  }
  fn magnitude (self) -> Option <S> {
    self.magnitude2().map (FixedSqrt::sqrt)
  }
  fn normalized (mut self) -> Self {
    let magnitude = loop {
      match self.magnitude() {
        Some (s) => break s,
        None => {
          let new = self.map (|s| s >> 1);
          self = Self::new (new.x, new.y);
        }
      }
    };
    // TODO: make this static or const ?
    let one = S::from_num (1usize);
    self.scale (one / magnitude)
  }
}

impl <S, U> Point2d <S, U> for euclid::Point2D <S, U> where S : FixedSqrt {
  fn new (x : S, y : S) -> Self {
    euclid::Point2D::new (x, y)
  }
  fn map <F, T> (self, mut f : F) -> euclid::Point2D <T, U> where
    F : FnMut (S) -> T
  {
    euclid::Point2D::new (f (self.x), f (self.y))
  }
}

impl <S, U> Vector2d <S, U> for euclid::Vector2D <S, U> where S : FixedSqrt {
  fn new (x : S, y : S) -> Self {
    euclid::Vector2D::new (x, y)
  }
  fn map <F, T> (self, mut f : F) -> euclid::Vector2D <T, U> where
    F : FnMut (S) -> T
  {
    euclid::Vector2D::new (f (self.x), f (self.y))
  }
  fn magnitude2 (self) -> Option <S> {
    let x2 = self.x.checked_mul (self.x)?;
    let y2 = self.y.checked_mul (self.y)?;
    x2.checked_add (y2)
  }
  fn norm (self) -> Norm <S> where S : FixedSigned {
    if let Some (magnitude) = self.magnitude() {
      return Norm::Euclidean (magnitude)
    }
    if let Some (sum) = self.x.checked_abs()
      .and_then (|x| self.y.checked_abs().and_then (|y| x.checked_add (y)))
    {
      return Norm::Taxicab (sum)
    }
    Norm::Maximum (self.x.saturating_abs().max (self.y.saturating_abs()))
  }
}

pub enum Norm <S> {
  /// 2-norm
  Euclidean (S),
  /// 1-norm
  Taxicab   (S),
  /// Infinity-norm
  Maximum   (S)
}

#[cfg(test)]
mod tests {
  use fixed;
  use euclid;
  use super::*;

  #[test]
  #[should_panic]
  fn test_overflow() {
    struct World;
    impl Space2d for World {
      type Scalar = fixed::types::I4F4;
      type Point  = euclid::Point2D  <Self::Scalar, Self>;
      type Vector = euclid::Vector2D <Self::Scalar, Self>;
    }
    type Scalar = <World as Space2d>::Scalar;
    type Point  = <World as Space2d>::Point;
    //type Vector = <World as Space2d>::Vector;

    let p = Point::new (Scalar::MIN, Scalar::MIN);
    let q = Point::new (Scalar::MAX, Scalar::MAX);
    show!(p);
    show!(q);
    show!(q - p);
    show!(p - q);
  }

  #[test]
  fn test_norm_positive() {
    struct World;
    impl Space2d for World {
      type Scalar = fixed::types::I4F4;
      type Point  = euclid::Point2D  <Self::Scalar, Self>;
      type Vector = euclid::Vector2D <Self::Scalar, Self>;
    }
    type Scalar = <World as Space2d>::Scalar;
    //type Point  = <World as Space2d>::Point;
    type Vector = <World as Space2d>::Vector;
    let mut x = Scalar::MIN;
    loop {
      let mut y = Scalar::MIN;
      loop {
        let norm = match Vector::new (x, y).norm() {
          Norm::Euclidean (n) => n,
          Norm::Taxicab   (n) => n,
          Norm::Maximum   (n) => n
        };
        show!(norm);
        assert!(norm >= Scalar::from_bits (0));
        if y == Scalar::MAX {
          break
        }
        y += Scalar::from_bits (1);
      }
      if x == Scalar::MAX {
        break
      }
      x += Scalar::from_bits (1);
    }
  }

}
