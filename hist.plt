set terminal png
set boxwidth 0.1
set style histogram rowstacked gap 0
set style fill solid 0.5 border lt -1
set output "hist.png"
plot 'hist.csv' smooth freq with boxes
