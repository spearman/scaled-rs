#!/bin/sh

set -x
set -e

cargo run --example norm
cat "norm.plt" | gnuplot
cat "hist.plt" | gnuplot
cat "diagonal.plt" | gnuplot
feh norm.png hist.png diagonal.png

exit 0
