with import <nixpkgs> {};
mkShell {
  buildInputs = [
    gdb  # required for rust-gdb
    gnuplot
    llvm
    rustup
    rust-analyzer
  ];
}
